#pragma once

template<typename T>
class com_ptr {
	T *ptr;

	void retain() {
		if(ptr)
			ptr->AddRef();
	}

	void release() {
		if(ptr)
			ptr->Release();
	}

public:
	com_ptr() 
	 : ptr() {
	}

	com_ptr(com_ptr const &other) 
	 : ptr(other.ptr) { 
		retain(); 
	}

	com_ptr& operator=(com_ptr const &other) {
		ptr = other.ptr; 
		retain();
		return *this;
	}

	~com_ptr() { 
		release();
	}

	T **operator&() { 
		return &ptr; 
	}

	T *get() { 
		return ptr; 
	}

	const T *get() const { 
		return ptr; 
	}

	T *operator->() { 
		return ptr; 
	}
};

typedef unsigned short pixel;

template<typename T>
struct point {
	T x, y;
};

template<typename T>
point<T> make_point(T x, T y) {
	point<T> point;
	point.x = x;
	point.y = y;
	return point;
}

template<typename T>
struct size {
	T width, height;
};

template<typename T>
size<T> make_size(T x, T y) {
	size<T> size;
	size.x = x;
	size.y = y;
	return size;
}

template<typename T>
struct frame {
	point<T> origin;
	size<T> size;

	bool contains(point<T> point) {
		return point.x > origin.x 
			&& point.x < origin.x + size.width 
			&& point.y > origin.y
			&& point.y < origin.y + size.height;
	}
};

template<typename T>
frame<T> make_frame(T x, T y, T width, T height) {
	frame<T> frame;
	frame.origin = make_point<T>(x, y);
	frame.size = make_size<T>(x, y);
	return frame;
}
