﻿#include "window.h"

point<pixel> make_point(POINTS point_s) {
	point<pixel> point;
	point.x = point_s.x;
	point.y = point_s.y;
	return point;
}

void main_window::register_window() {
	WNDCLASSEXW wcex = WNDCLASSEXW();
	wcex.cbSize = sizeof(WNDCLASSEXW);
    wcex.style = CS_HREDRAW | CS_VREDRAW;
    wcex.lpfnWndProc = window_proc;
    wcex.cbWndExtra = sizeof(LONG_PTR);
    wcex.hInstance = GetModuleHandleW(0);
    wcex.hCursor = LoadCursorA(0, IDC_ARROW);
    wcex.lpszClassName = L"🍌";
    RegisterClassExW(&wcex);
}

main_window::main_window() {
	D2D1CreateFactory(D2D1_FACTORY_TYPE_SINGLE_THREADED, &d2d_factory);

	hwnd = CreateWindowExW(0, L"🍌", L"🍌🍌🍌", 
		WS_OVERLAPPEDWINDOW, CW_USEDEFAULT, CW_USEDEFAULT,
        800, 600, 0, 0, GetModuleHandleW(0), 0);
	SetWindowLongW(hwnd, GWLP_USERDATA, PtrToUlong(this));

	d2d_factory->CreateHwndRenderTarget(D2D1::RenderTargetProperties(), D2D1::HwndRenderTargetProperties(hwnd), &render_target);
}

void main_window::add_view(std::shared_ptr<view> view) {
	views.push_back(view);
}

void main_window::show() {
	ShowWindow(hwnd, SW_SHOWNORMAL);
}

void main_window::update() {
	UpdateWindow(hwnd);
}

LRESULT main_window::resize(UINT32 width, UINT32 height) {
	D2D1_SIZE_U size = { width, height };
	render_target->Resize(size);
	return 0;
}

LRESULT main_window::display_changed() {
	InvalidateRect(hwnd, 0, false);
	return 0;
}

LRESULT main_window::paint() {
	render_target->BeginDraw();
	render_target->Clear(D2D1::ColorF(D2D1::ColorF::White));

	::render_target target(render_target);

	for(std::shared_ptr<view>& view : views)
		view->paint(target);
	
	render_target->EndDraw();

	ValidateRect(hwnd, 0);

	return 0;
}

LRESULT main_window::destroy() {
	PostQuitMessage(0);
	return 0;
}

LRESULT main_window::clicked(POINTS point_s) {
	point<pixel> point = make_point(point_s);

	for(auto view : views)
		if(view->frame.contains(point))
			view->clicked(point);
	
	InvalidateRect(hwnd, 0, false);
	return 0;
}

LRESULT CALLBACK main_window::window_proc(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam) {
	main_window *self = reinterpret_cast<main_window *>(static_cast<LONG_PTR>(GetWindowLongW(hwnd, GWLP_USERDATA)));

	if(message == WM_SIZE)
		return self->resize(LOWORD(lParam), HIWORD(lParam));
	if(message == WM_DISPLAYCHANGE)
		return self->display_changed();
	else if(message == WM_PAINT)
		return self->paint();
	else if(message == WM_DESTROY)
		return self->destroy();
	else if(message == WM_LBUTTONDOWN)
		return self->clicked(MAKEPOINTS(lParam));
	else
		return DefWindowProcW(hwnd, message, wParam, lParam);
}

view::view() : frame() {}

render_target::render_target(com_ptr<ID2D1HwndRenderTarget> t_)
 : d2d_render_target(t_) {
}

void render_target::draw_line(D2D1::ColorF color, point<pixel> start, point<pixel> end) {
	com_ptr<ID2D1SolidColorBrush> brush;
	d2d_render_target->CreateSolidColorBrush(color, &brush);

	D2D1_POINT_2F start_f = { start.x, start.y };
	D2D1_POINT_2F end_f = { end.x, end.y };
	d2d_render_target->DrawLine(start_f, end_f, brush.get());
}

void render_target::draw_rect(D2D1::ColorF color, frame<pixel> frame) {
	com_ptr<ID2D1SolidColorBrush> brush;
	d2d_render_target->CreateSolidColorBrush(color, &brush);

	D2D1_RECT_F rect = { frame.origin.x, frame.origin.y, frame.origin.x + frame.size.width, frame.origin.y + frame.size.height };
	d2d_render_target->DrawRectangle(rect, brush.get());
}

void render_target::draw_bitmap(bound_bitmap &bitmap, point<pixel> origin) {
	auto bitmap_size = bitmap->GetSize();
	D2D1_RECT_F rect = { origin.x, origin.y, origin.x + bitmap_size.width, origin.y + bitmap_size.height };

	d2d_render_target->DrawBitmap(bitmap.get(), rect);
}

