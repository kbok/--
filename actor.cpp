#include "actor.h"

#include <sstream>

board_view::board_view() {
	frame.size.width = 800;
	frame.size.height = 600;
}

void board_view::paint(render_target &render_target) {
	D2D1::ColorF color = D2D1::ColorF(D2D1::ColorF::Orange);

	for(unsigned x = 0; x < frame.size.width; x += 64) 
		render_target.draw_line(color, make_point<pixel>(x, 0), make_point<pixel>(x, frame.size.height));

	for(unsigned y = 0; y < frame.size.height; y+= 64)
		render_target.draw_line(color, make_point<pixel>(0, y), make_point<pixel>(frame.size.width, y));
}

void board_view::clicked(point<pixel> point) {
}

actor_view::actor_view(asset_library &l, actor &a) 
 : library(l)
 , model(a)
 , selected()
 , animator() {
	 frame.size.width = 64;
	 frame.size.height = 64;
}

static void draw_from_hotspot(render_target &render_target, bound_bitmap &bitmap, point<pixel> origin) {
	origin.x -= bitmap.hotspot_x;
	origin.y -= bitmap.hotspot_y;

	render_target.draw_bitmap(bitmap, origin);
}

void actor_view::paint(render_target &render_target) {
	animator++;

	frame.origin.x = model.coordinates.x * 64;
	frame.origin.y = model.coordinates.y * 64;

	point<pixel> center = make_point<pixel>(frame.origin.x + 32, frame.origin.y + 32);

	if(model.type == actor_type_tank) {
		draw_from_hotspot(render_target, library["TANK0"], center);
		draw_from_hotspot(render_target, library["TANK12"], center);
	} else if (model.type == actor_type_assault) {
		draw_from_hotspot(render_target, library["ARTILLRY5"], center);
	} else if (model.type == actor_type_scanner) {
		unsigned animation_state = 8 + 2 * ((animator / 8) % 8);
		std::stringstream ss;
		ss << "SCANNER" << animation_state;
		draw_from_hotspot(render_target, library["SCANNER2"], center);
		draw_from_hotspot(render_target, library[ss.str()], center);
	}

	if(selected)
		render_target.draw_rect(D2D1::ColorF(D2D1::ColorF::Yellow), frame);
}

void actor_view::clicked(point<pixel> point) {
	selected = !selected;
}
