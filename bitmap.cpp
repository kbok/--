#include "bitmap.h"

#include <sstream>

IWICImagingFactory *bitmap::wic_factory;

std::string read_metadata(IWICMetadataQueryReader *reader, std::wstring const &name) {
	std::string result;

	PROPVARIANT variant;
	PropVariantInit(&variant);
	HRESULT hr = reader->GetMetadataByName(name.c_str(), &variant);

	if (variant.vt == VT_LPSTR)
		result = variant.pszVal;

	PropVariantClear(&variant);

	return result;
}

bitmap::bitmap() : hotspot_x(), hotspot_y(), frame() {
	if(!wic_factory)
		CoCreateInstance(CLSID_WICImagingFactory, 0, CLSCTX_INPROC_SERVER, IID_IWICImagingFactory, reinterpret_cast<void **>(&wic_factory));
	else
		wic_factory->AddRef();
}

bitmap::bitmap(std::wstring const &filename) {
	if(!wic_factory)
		CoCreateInstance(CLSID_WICImagingFactory, 0, CLSCTX_INPROC_SERVER, IID_IWICImagingFactory, reinterpret_cast<void **>(&wic_factory));
	else
		wic_factory->AddRef();

	com_ptr<IWICBitmapDecoder> decoder;
	wic_factory->CreateDecoderFromFilename(filename.c_str(), 0, GENERIC_READ, WICDecodeMetadataCacheOnDemand, &decoder);
	decoder->GetFrame(0, &frame);

	com_ptr<IWICMetadataQueryReader> metadata_reader;
	frame->GetMetadataQueryReader(&metadata_reader);

	char comma;
	std::stringstream strstr(read_metadata(metadata_reader.get(), L"/tEXt/hotspot"));
	strstr >> hotspot_x >> comma >> hotspot_y;
}

bitmap::~bitmap() {
	wic_factory->Release();
}

bound_bitmap bitmap::bind(ID2D1HwndRenderTarget &render_target) {
	com_ptr<IWICFormatConverter> converter;
	wic_factory->CreateFormatConverter(&converter);
	converter->Initialize(frame.get(), GUID_WICPixelFormat32bppPBGRA, WICBitmapDitherTypeNone, 0, 0.f, WICBitmapPaletteTypeMedianCut);

	bound_bitmap bound_bitmap;
	render_target.CreateBitmapFromWicBitmap(converter.get(), 0, &bound_bitmap.d2d_bitmap);
	bound_bitmap.hotspot_x = hotspot_x;
	bound_bitmap.hotspot_y = hotspot_y;

	return bound_bitmap;
}

void asset_library::load() {
	for(unsigned i = 0; i < 24; ++i) {
		std::wstringstream filename_ss;
		std::stringstream ident_ss;
		filename_ss << L"output/SCANNER_" << i << L".png";
		ident_ss << "SCANNER" << i;
		bitmaps[ident_ss.str()] = std::make_shared<bitmap>(filename_ss.str());
	}

	bitmaps["TANK0"] = std::make_shared<bitmap>(L"output/TANK_0.png");
	bitmaps["TANK12"] = std::make_shared<bitmap>(L"output/TANK_12.png");
	bitmaps["ARTILLRY5"] = std::make_shared<bitmap>(L"output/ARTILLRY_5.png");
}

void asset_library::bind(com_ptr<ID2D1HwndRenderTarget> render_target) {
	for(auto entry : bitmaps) {
		auto bound = std::make_shared<bound_bitmap>();
		*bound = entry.second->bind(*render_target.get());
		bound_bitmaps[entry.first] = bound;
	}
}

bound_bitmap asset_library::operator[](std::string const &ident) {
	return *bound_bitmaps[ident];
}