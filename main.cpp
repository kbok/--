﻿#include "bitmap.h"
#include "window.h"
#include "actor.h"

struct com_ensure_initialized {
	com_ensure_initialized() { CoInitialize(0); }
	~com_ensure_initialized() { CoUninitialize(); }
};

int WINAPI WinMain(
    HINSTANCE /*hInstance*/,
    HINSTANCE /*hPrevInstance*/,
    LPSTR /*lpCmdLine*/,
    int /*nCmdShow*/
    )
{
	com_ensure_initialized com_initialize;
    
	asset_library library;
	library.load();

	main_window::register_window();
	main_window window;
	library.bind(window.render_target);

	actor tank0;
	tank0.type = actor_type_tank;
	tank0.coordinates.x = 3;
	tank0.coordinates.y = 4;

	actor tank1;
	tank1.type = actor_type_scanner;
	tank1.coordinates.x = 5;
	tank1.coordinates.y = 4;

	actor assault0;
	assault0.type = actor_type_assault;
	assault0.coordinates.x = 6;
	assault0.coordinates.y = 6;

	window.add_view(std::shared_ptr<view>(new board_view()));
	window.add_view(std::shared_ptr<view>(new actor_view(library, tank0)));
	window.add_view(std::shared_ptr<view>(new actor_view(library, tank1)));
	window.add_view(std::shared_ptr<view>(new actor_view(library, assault0)));

	window.show();
	window.update();

	MSG msg;

    do {
		window.paint();

		if(PeekMessageW(&msg, 0, 0, 0, PM_REMOVE)) {
			TranslateMessage(&msg);
			DispatchMessageW(&msg);
		}
        
	} while(msg.message != WM_QUIT);

    return 0;
}

