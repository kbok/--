#pragma once

#include <string>
#include <map>
#include <memory>

#include <wincodec.h>
#include <d2d1.h>

#include "util.h"

struct bound_bitmap {
	unsigned hotspot_x, hotspot_y;
	com_ptr<ID2D1Bitmap> d2d_bitmap;

	ID2D1Bitmap *get() { return d2d_bitmap.get(); }
	ID2D1Bitmap *operator->() { return d2d_bitmap.operator->(); }
};

class bitmap {
	static IWICImagingFactory *wic_factory;

	unsigned hotspot_x, hotspot_y;
	com_ptr<IWICBitmapFrameDecode> frame;

public:
	bitmap();
	bitmap(std::wstring const &filename);
	~bitmap();

	bound_bitmap bind(ID2D1HwndRenderTarget &render_target);
};

class asset_library {
	std::map<std::string, std::shared_ptr<bitmap>> bitmaps;
	std::map<std::string, std::shared_ptr<bound_bitmap>> bound_bitmaps;

public:
	void load();
	void bind(com_ptr<ID2D1HwndRenderTarget> render_target);

	bound_bitmap operator[](std::string const &ident);
};
