#pragma once

#include "window.h"

struct board_coordinates {
	unsigned x, y;
};

enum actor_type {
	actor_type_tank,
	actor_type_assault,
	actor_type_scout,
	actor_type_scanner
};

struct actor {
	actor_type type;
	board_coordinates coordinates;
};

class board_view : public view {
public:
	board_view();

	void paint(render_target &);
	void clicked(point<pixel> point);
};

class actor_view : public view {
	actor& model;
	asset_library &library;
	bool selected;
	unsigned char animator;

public:
	static void load_assets(com_ptr<ID2D1HwndRenderTarget> render_target);

	actor_view(asset_library&, actor&);

	void paint(render_target &);
	void clicked(point<pixel> point);
};
