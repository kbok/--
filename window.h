#pragma once

#include <memory>
#include <vector>

#include <windows.h>
#include <d2d1.h>

#include "util.h"
#include "bitmap.h"

class render_target {
	com_ptr<ID2D1HwndRenderTarget> d2d_render_target;

public:
	render_target(com_ptr<ID2D1HwndRenderTarget>);

	void draw_line(D2D1::ColorF color, point<pixel> start, point<pixel> end);
	void draw_rect(D2D1::ColorF color, frame<pixel> frame);
	void draw_bitmap(bound_bitmap &bitmap, point<pixel> origin);
};

class view {
public:
	frame<pixel> frame;

	view();

	virtual void paint(render_target &) = 0;
	virtual void clicked(point<pixel> point) = 0;

	virtual ~view() = 0 {};
};

class main_window {
	static LRESULT CALLBACK window_proc(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam);

	com_ptr<ID2D1Factory> d2d_factory;
	std::vector<std::shared_ptr<view>> views;
	
	HWND hwnd;

public:
	com_ptr<ID2D1HwndRenderTarget> render_target;
	
	static void register_window();

	main_window();
	void show();
	void update();

	void add_view(std::shared_ptr<view>);
	HWND handle() { return hwnd; }

	// Dispatched
	LRESULT resize(UINT32 width, UINT32 height);
	LRESULT display_changed();
	LRESULT paint();
	LRESULT destroy();
	LRESULT clicked(POINTS point);
};

